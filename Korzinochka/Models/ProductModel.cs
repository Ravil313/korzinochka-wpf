﻿using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Korzinochka.Models
{
    internal class ProductModel
    {
        public string Name { get; set; }           
        public double Price { get; set; }
        public int Count { get; set; }
        public double TotalPrice { get; set; }
    }
}
