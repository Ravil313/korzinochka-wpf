﻿using System;
using System.Collections.ObjectModel;

namespace Korzinochka.Models
{
    internal class BasketModel
    {
        public DateTime? PurchaseDate { get; set; }
        public double TotalInBasket { get; set; }
        public ObservableCollection<ProductModel> Products { get; set; }
    }
}
