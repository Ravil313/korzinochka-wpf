﻿using System.Collections.ObjectModel;

namespace Korzinochka.Models
{
    internal class HistoryModel
    {
        public ObservableCollection<BasketModel> BasketInHistory { get; set; }
    }
}
