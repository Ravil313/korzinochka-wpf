﻿using Korzinochka.Infrastructure.Commands;
using Korzinochka.Models;
using Korzinochka.ViewModels.Base;
using System;
using System.Collections.ObjectModel;
using System.Windows.Input;
using System.Windows.Media.Animation;

namespace Korzinochka.ViewModels
{
    internal class MainWindowViewModel : ViewModel
    {
        /*Свойства-----------------------------------------------------------------------------------------------------------*/
        #region Title
        private string _title = "Корзиночка";
        /// <summary>Заголовок окна</summary>
        public string Title
        {
            get => _title;
            set => Set(ref _title, value);
        }
        #endregion
        /*-------------------------------------------------------------------------------------------------------------------*/
        #region Товары
        private ObservableCollection<ProductModel> _products = new ObservableCollection<ProductModel>
        {
            new ProductModel {Name = "Чай", Price = 60.0},
            new ProductModel {Name = "Сахар", Price = 110.5},
            new ProductModel {Name = "Конфеты", Price = 218.3},
            new ProductModel {Name = "Хлеб", Price = 32.0}
        };
        /// <summary>Список товаров</summary>
        public ObservableCollection<ProductModel> Products
        {
            get => _products;
            set => Set(ref _products, value);
        }
        #endregion
        /*-------------------------------------------------------------------------------------------------------------------*/
        #region Покупка
        #region PurchaseDate
        private DateTime _purchaseDate = System.DateTime.Now;
        /// <summary>Дата покупки</summary>
        public DateTime PurchaseDate
        {
            get => _purchaseDate;
            set => Set(ref _purchaseDate, value);
        }
        #endregion
        /*-------------------------------------------------------------------------------------------------------------------*/
        #region SelectedProductInProducts
        private ProductModel _selectedProductInProducts;
        /// <summary>Выбранный продукт в товарах</summary>
        public ProductModel SelectedProductInProducts
        {
            get => _selectedProductInProducts;
            set
            {
                Set(ref _selectedProductInProducts, value);
                TotalPriceOfSelectedProduct = _countOfSelectedProduct * _selectedProductInProducts.Price;
            }
        }
        #endregion
        /*-------------------------------------------------------------------------------------------------------------------*/
        #region SelectedProductInBasket
        private ProductModel _selectedProductInBasket;
        /// <summary>Выбранный продукт в корзине</summary>
        public ProductModel SelectedProductInBasket
        {
            get => _selectedProductInBasket;
            set => Set(ref _selectedProductInBasket, value);
        }
        #endregion
        /*-------------------------------------------------------------------------------------------------------------------*/
        #region CountOfSelectedProduct
        private int _countOfSelectedProduct = 1;
        /// <summary>Количество выбранного продукта</summary>
        public int CountOfSelectedProduct
        {
            get => _countOfSelectedProduct;
            set
            {
                Set(ref _countOfSelectedProduct, value);
                if (_selectedProductInProducts == null) return;
                TotalPriceOfSelectedProduct = value * _selectedProductInProducts.Price;
            }
        }
        #endregion
        /*-------------------------------------------------------------------------------------------------------------------*/
        #region TotalPriceOfSelectedProduct
        private double _totalPriceOfSelectedProduct = 0;
        /// <summary>Общая</summary>
        public double TotalPriceOfSelectedProduct
        {
            get => _totalPriceOfSelectedProduct;
            set => Set(ref _totalPriceOfSelectedProduct, value);
        }
        #endregion
        /*-------------------------------------------------------------------------------------------------------------------*/
        #region Basket
        private BasketModel _basket = new BasketModel()
        {
            PurchaseDate = DateTime.Now,
            TotalInBasket = 333,
            Products = new ObservableCollection<ProductModel>()
            {
                new ProductModel()
                {
                    Name = "Пельмени",
                    Price = 777,
                    Count = 1,
                    TotalPrice = 777,
                }
            }
        };
        /// <summary>Корзина</summary>
        public BasketModel Basket
        {
            get => _basket;
            set => Set(ref _basket, value);
        }
        #endregion
        /*-------------------------------------------------------------------------------------------------------------------*/
        #region TotalInBasket
        private double _totalInBasket = 0;
        /// <summary>Итого в корзине</summary>
        public double TotalInBasket
        {
            get => _totalInBasket;
            set => Set(ref _totalInBasket, value);
        }
        #endregion
        #endregion
        /*-------------------------------------------------------------------------------------------------------------------*/
        #region История
        private ObservableCollection<BasketModel> _history = new ObservableCollection<BasketModel>()
        {
            new BasketModel()
            {
                PurchaseDate = DateTime.Now.AddDays(10000),
                TotalInBasket = 777,
                Products = new ObservableCollection<ProductModel>()
                {
                    new ProductModel()
                    {
                        Name = "Пельмени",
                        Price = 777,
                        Count = 1,
                        TotalPrice = 777,
                    }
                }
            }
        };
        /// <summary>Список в истории</summary>
        public ObservableCollection<BasketModel> History
        {
            get => _history;
            set => Set(ref _history, value);
        }
        #endregion
        /*-------------------------------------------------------------------------------------------------------------------*/
        #region SelectedProductInHistory
        private ObservableCollection<BasketModel> _selectedProductInHistory;
        /// <summary>Выбранный продукт в истории</summary>
        public ObservableCollection<BasketModel> SelectedProductInHistory
        {
            get => _selectedProductInHistory;
            set => Set(ref _selectedProductInHistory, value);
        }
        #endregion
        /*-------------------------------------------------------------------------------------------------------------------*/
        #region Команды
        #region AddInBasketCommand
        /// <summary>Команда добавление выбранного товара в корзину</summary>
        public ICommand AddInBasketCommand { get; }
        private bool CanAddInBasketCommandExecute(object p) => true;
        private void OnAddInBasketCommandExecuted (object p)
        {
            if(_selectedProductInProducts == null || _countOfSelectedProduct <= 0) return;
            Basket.PurchaseDate = PurchaseDate;            
            SelectedProductInProducts.TotalPrice = TotalPriceOfSelectedProduct;
            SelectedProductInProducts.Count = CountOfSelectedProduct;
            TotalInBasket += SelectedProductInProducts.TotalPrice;
            Basket.Products.Add(SelectedProductInProducts);
        }
        #endregion
        /*-------------------------------------------------------------------------------------------------------------------*/
        #region DeleteFromBasketCommand
        /// <summary>Команда удаления выбранного товара из корзины</summary>
        public ICommand DeleteFromBasketCommand { get; }
        private bool CanDeleteFromBasketCommandExecute(object p) => p is ProductModel basketItem && Basket.Products.Contains(basketItem);
        private void OnDeleteFromBasketCommandExecuted(object p)
        {
            if (!(p is ProductModel basketItem)) return;  
            var itemIndex = Basket.Products.IndexOf(basketItem);
            Basket.Products.Remove(basketItem);
            TotalInBasket -= basketItem.TotalPrice;
            if(TotalInBasket < 1)
                TotalInBasket = 0.0;
            if (itemIndex < Basket.Products.Count)
                SelectedProductInBasket = Basket.Products[itemIndex];
        }
        #endregion
        #region SaveInHistoryCommand
        /// <summary>Команда сохранеия покупки в историю</summary>
        public ICommand SaveInHistoryCommand { get; }
        private bool CanSaveInHistoryCommandExecute(object p) => true;
        private void OnSaveInHistoryCommandExecuted(object p)
        {
            if (Basket == null) return;
            Basket.TotalInBasket = TotalInBasket;
            History.Add(Basket);
        }
        #endregion
        /*-------------------------------------------------------------------------------------------------------------------*/
        #endregion
        public MainWindowViewModel()
        {
            AddInBasketCommand = new LambdaCommand(OnAddInBasketCommandExecuted, CanAddInBasketCommandExecute);
            DeleteFromBasketCommand = new LambdaCommand(OnDeleteFromBasketCommandExecuted, CanDeleteFromBasketCommandExecute);
            SaveInHistoryCommand = new LambdaCommand(OnSaveInHistoryCommandExecuted, CanSaveInHistoryCommandExecute);
        }
    }
}
